export const fakeData = {};

// fake fetch with in memory data store
export const fakeFetch = key => new Promise((resolve) => {
  setTimeout(() => {
    resolve(fakeData[key]);
  }, 500);
})