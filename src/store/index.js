import createSagaMiddleware from 'redux-saga'
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'

import api from '../core/api'
import createAppStore from './createAppStore'
import reducer from './reducer'

const sagaMiddleware = createSagaMiddleware();
const store = createAppStore(reducer, sagaMiddleware);

function* handle(action) {
  try {
    if (!action.name) {
      throw 'Action name is not defined';
    }

    const url = action.name;
    const data = yield call(u => api.post(u, action.data), url);

    yield put({type: "HandleSucceeded", name: action.name, data: data});
  } catch (e) {
    yield put({type: "HandleFailed", message: e.message});
  }
}

function* ask(action) {
  try {
    if (!action.name) {
      throw 'Action name is not defined';
    }

    yield put({type: "AskStarted", name: action.name});
    const data = yield call(t => api.get(t.name, t.data), action);
    yield put({type: "AskSucceeded", name: action.name, data});
  } catch (e) {
    yield put({type: "AskFailed", message: e.message});
  }
}
function* cqrsSaga() {
    yield takeEvery("Ask", ask);
    yield takeEvery("Handle", handle);
}

sagaMiddleware.run(cqrsSaga);
export default store;