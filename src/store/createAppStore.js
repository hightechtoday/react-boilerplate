import { createStore, applyMiddleware, compose } from 'redux';

// redux dev tools browser extension
const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const createAppStore = (reducer, ...middleware) => {
  // store with saga and dev tools support
  const store = createStore(reducer, composeEnhancers(applyMiddleware(...middleware)));
    // reset reducers via HMR
    if (module.hot) {
        module.hot.accept('./reducer', () =>
            store.replaceReducer(require('./reducer').default)
        );
    }
    return store;
}

export default createAppStore;