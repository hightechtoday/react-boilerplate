import React from 'react'
import { Link } from 'react-router'
export default (props = {title: 'Hello World'}) => (
  <div>
    <h1>{props.title}</h1>
    <ul>
      <li><Link to="/about">About</Link></li>
      <li><Link to="/users">Users</Link></li>
      <li><Link to="/user/3">User 3</Link></li>
    </ul>
    <hr/>
    {props.children}
  </div>
)