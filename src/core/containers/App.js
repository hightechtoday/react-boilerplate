import App from '../components/App'
import { connect } from 'react-redux'

const mapStateToProps = state => { return {title: state.App.title} };

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      dispatch({
        type: 'AppClicked',
        'title': ownProps.title
      })
    }
  }
};
const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default AppContainer;