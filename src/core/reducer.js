export default (state = {title: 'Hello World'}, action) => {
  switch (action.type) {
    case 'ChangeTitle':
      return action.title;
    default:
      return state
  }
}