import React from 'react'
import { Router, Route, Link, hashHistory  } from 'react-router'
import App from './core/containers/App'
import About from './modules/About'
import NotFound from './modules/NotFound'
import Users, {User} from './Modules/Users'

export default props => (
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <Route path="/about" component={About}/>
      <Route path="/users" component={Users} />
      <Route path="/user/:id" component={User}/>
      <Route path="*" component={NotFound}/>
      </Route>
   </Router>
)